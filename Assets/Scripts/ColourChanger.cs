﻿using UnityEngine;
using System.Collections;

public class ColourChanger : MonoBehaviour {

    Rigidbody rb;
    Renderer myRenderer;
    GameObject parentObj;
    Color[] colors = { Color.green, Color.blue, Color.cyan, Color.yellow, Color.white, Color.magenta, Color.red };

    IEnumerator ColourChange()
    {
        int ite = 0;
        while(true)
        {
            Debug.Log(ite);
            if(ite == 5)
            {
                Destroy(parentObj);
            }
            rb.AddForce(Vector3.up * 10f);
            rb.AddForce(Vector3.right * 5f);
            parentObj.transform.Rotate(Random.Range(0,5), Random.Range(0, 5), Random.Range(0, 10));
            int index = (int)Random.Range(0, colors.Length);
            float eT = 0f;
            float tT = 5f;
            while (eT < tT) {
                eT += Time.deltaTime;
                myRenderer.material.color = Color.Lerp(myRenderer.material.color, colors[index], (eT/tT));
                yield return null;
            }
            ite++;
            yield return new WaitForSeconds(5);
        }
    }

	void Start () {
        rb = GetComponentInParent<Rigidbody>();
        parentObj = transform.parent.gameObject;
        myRenderer = GetComponent<Renderer>();
        StartCoroutine("ColourChange");	
	}
}
