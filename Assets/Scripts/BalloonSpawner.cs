﻿using UnityEngine;
using System.Collections;

public class BalloonSpawner : MonoBehaviour {

    public GameObject balloon;
    Color[] colors = { Color.green, Color.blue, Color.cyan, Color.yellow, Color.white, Color.magenta, Color.red };

    void Start () {
        int index = (int)Random.Range(0, colors.Length);
        StartCoroutine("Countdown");
	}

    IEnumerator Countdown()
    {
        while (true)
        {
            SpawnNew();
            SpawnNew();
            yield return new WaitForSeconds(20);
        }
    }

    public void SpawnNew()
    {
        Vector3 newPos = transform.position;
        Vector3 offset = new Vector3(Random.Range(5, 50), Random.Range(1, 25), Random.Range(5, 50));
        GameObject newBal = GameObject.Instantiate(balloon, newPos + offset, Quaternion.identity) as GameObject;
        newBal.transform.parent = transform;
    }
}
