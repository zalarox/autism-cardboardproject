﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

	public void StartBubbles()
    {
        SceneManager.LoadScene("bubbles-scene", LoadSceneMode.Single);
    }

    public void StartFireworks()
    {
        SceneManager.LoadScene("fireworks-scene", LoadSceneMode.Single);
    }
}
